#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <termios.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

#include "linenoise/linenoise.h"

#include "discord.h"
#include "proj_parse.h"
#include "config.h"
#include "share.h"
#include "util.h"

#define DEBUG_print_args(argc,argv)

static int longest_command_len;

enum command_info {
	HELP_oneline = 1,
	HELP_full,
	HELP_explain,
};

struct command_help {
	char **argv;
	char *usage;
	char *explain;
	char **opt_explain;
	enum command_info info;
};

static int print_help(struct command_help h)
{
	switch(h.info) {
	case HELP_full:
		printf(
			"usage: %s %s\n"
			"%s\n\n"
			"options:\n",
			*h.argv, h.usage, h.explain
		);
		for(; **h.opt_explain; ++h.opt_explain) {
			printf("%s\n", *h.opt_explain);
		}
		return 0;
	case HELP_oneline:
		printf("usage: %s %s\n", *h.argv, *h.usage);
		return 0;
	case HELP_explain:
		printf("%-*s - %s\n", longest_command_len, *h.argv, h.explain);
		return 0;
	default:
		return -1;
	}
}

static int peek(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"-d - dunny", "-b - bunny", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "an example function.",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	printf("a boo\n");
	return 0;
}

static int beak(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"-d - dunny", "-b - bunny", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "an example function.",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	printf("a poo\n");
	return 0;
}

static int print_args(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "prints its arguments.",
			.opt_explain = opt,
		};
		print_help(help);
		return 0;
	}
	printf("argc: %d\n", argc);
	if(argv) {
		printf("argv: ");
		for(; *argv; ++argv) {
			printf("'%s'", *argv);
		}
		printf("\n");
	}
	fflush(stdout);
	return 0;
}

static void debug_print_args(int argc, char *argv[])
{
	printf("DEBUG\n");
	print_args(argc, argv, 0);
	printf("DEBUG\n");
}

static int quit(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "stop running.",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	printf("quitting\n");
	share->run = 0;
	return 0;
}

static int print_info(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "print the info until q is pressed.",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	puts("press any key to exit");
	struct termios term, term_save;
	tcgetattr(STDIN_FILENO, &term);
	tcgetattr(STDIN_FILENO, &term_save);
	term.c_lflag &= ~(ICANON);
	term.c_cc[VMIN] = 0;
	term.c_cc[VTIME] = 1;
	tcsetattr(STDIN_FILENO, TCSANOW, &term);
	while(getchar() == -1)
		printf("\033[1G\033[2K%d ", share->num);
	printf("\n");
	tcsetattr(STDIN_FILENO, TCSANOW, &term_save);
	return 0;
}

static int p_file_list(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "print all file info",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	print_file_list();
}

static int print_discord_presence(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "",
			.explain = "print the current discordPresence",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	print_presence();
}

static int help(int argc, char *argv[], enum command_info info);

static struct command_dict {
	int (*cmd)(int, char**, enum command_info);
	char name[32];
} command[] = {
	{peek,       "peek" },
	{beak,       "beak" },
	{print_args, "print"},
	{help,       "help" },
	{quit,       "quit" },
	{print_info, "info" },
	{p_file_list, "file" },
	{print_discord_presence, "pres" },
	{quit,       "q"    },
	{peek,       "p"    },
	{0,          0      },
};

static int find_longest_cmd_len(void)
{
	int ret = 0;
	struct command_dict *i = command;
	for(; *i->name; ++i) {
		int len = strlen(i->name);
		if(len > ret)
			ret = len;
	}
	return ret;
}

static int exec_cmd(int argc, char *argv[], enum command_info inf )
{
	struct command_dict *d = command;
	for(;*d->name; ++d) {
		if(!strncmp(argv[0], d->name, strlen(d->name))) {
			if(d->cmd(argc, argv, inf))
				printf("Error");
			break;
		}
	}
	if(!*d->name && !inf) {
		printf("command not found try: help\n");
		return -1;
	}
	if(!*d->name) {
		printf("Could not find help for: %s\n", *argv);
	}
	return 0;
}

static int help(int argc, char *argv[], enum command_info info)
{
	if(info) {
		char *opt_exp[] = {"this command has no options.", ""};
		struct command_help help = {
			.info    = info,
			.argv    = argv,
			.usage   = "[command]",
			.explain = "displays command help.",
			.opt_explain = opt_exp,
		};
		print_help(help);
		return 0;
	}
	if(!argc)
		return -1;
	struct command_dict *d = command;
	if(argc == 1) {
		printf("avalable commands\n");
		for(; *d->name; ++d) {
			char *o[2] = {d->name, ""};
			d->cmd(2, o, HELP_explain);
		}
		return 0;
	}
	if(argc < 2) {
		printf("Not enough args\n");
		return -1;
	}
	exec_cmd(argc - 1, &argv[1], HELP_full);
	return 0;
}

#define MAX_ARGS 32

static void p_strings(char *s[MAX_ARGS])
{
		printf("p_strings\n");
	for(int i = 0; i < MAX_ARGS && *s; ++i)
		printf("%s\n", *s++);
}

/*
 * manipulate first argument from str into the next position of arg
 *
 * return negitive on error
 * return 0 on success
 * return 1 on no match
 */
static int parse_one(char ***arg, char **str)
{
	while(isspace(**str)) {
		(*str)++;
	}
	if(!**str)
		return 1;

	if(**str != '"') {
		**arg = *str;
		(*arg)++;
		while(**str && !isspace(**str)) {
			if(**str == '"')
				return -1;
			(*str)++;
		}
		**str = '\0';
		(*str)++;
		return 0;
	}
	++(*str);
	if(!**str)
		return 0;
	**arg = *str;
	(*arg)++;
	int c = 0;
	while(**str && **str != '"') {
		++(*str);
		++c;
	}
	if(!c)
		return 1;
	if(**str) {
		**str = '\0';
		(*str)++;
		return 0;
	}
	return -1;
}

static int str_parse(char *s, char ***out, int *argc)
{
	int count = 0;
	static char *ret[MAX_ARGS] = {0};
	char **r = ret;
	int parse_error = parse_one(&r, &s);
	if(parse_error < 0)
		goto mismatch;
	if(!parse_error)
		count++;
	else
		goto empty;
	while(*s && ((ret - r) < MAX_ARGS) && *s != '\n') {
		if((parse_error = parse_one(&r, &s)) < 0)
			goto mismatch;
		if(!parse_error)
			count++;
	}
	if(*s == '\n')
		*s = '\0';
	*r = NULL;
	*out = ret;
	*argc = count;
	return 0;
mismatch:
	printf(
		"Please use: whitespace\"token\"whitespace\n"
		"If that does not work, it's a bug. Please report it.\n"
	);
	*out = NULL;
	*argc = 0;
	return -1;
empty:
	*out = NULL;
	*argc = 0;
	return 1;
}

int repl_main(void (*signal_handler)(int))
{
	sigint_handle_reg(signal_handler);
	longest_command_len = find_longest_cmd_len();

	while(share->run) {
/* public */
		printf("repl_main run=%d\n", share->run);
		//getchar();
		char *line = linenoise("sdcl>");
		if(!line || !*line)
			continue;
		linenoiseHistoryAdd(line);
		linenoiseHistorySave("history.txt");
		char **argv;
		int argc;
		str_parse(line, &argv, &argc);
		DEBUG_print_args(argc, argv);
		if(!argc) {
			char *o[] = {"help", ""};
			help(1, o, 0);
			continue;
		}
		exec_cmd(argc, argv, 0);
		free(line);
	}
	return 0;
}
