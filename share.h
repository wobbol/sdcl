#ifndef SHARE_H
#define SHARE_H
#include <stdint.h>
#include <time.h>


struct file {
	char name[FILENAME_MAX];
	char proj[FILENAME_MAX];
	char path[FILENAME_MAX];
	time_t start;
	time_t len;
	off_t line;
	int is_open;
	int was_open;
	uintmax_t uniq;
};
struct sha {
	int num;
	int run;
	struct file file_list[MAX_OPEN_FILES];
} share_buf;
struct sha *share;

struct sha *init_share(void);
int64_t StartTime;

#endif
