#define PRJ_BUF_SZ   64
#define MAX_PROJECTS 256

#define MAX_OPEN_FILES 10

#define code_path         "%home%username%code%"
#define popen_command     "lsof +D /home/username/.local/share/nvim/swap -Fn"
#define project_header    "Open"
#define no_project        "None"
#define committing_header "Commit"
#define state_editing     "Editing"
#define state_not_editing "Idleing"

#define sleep_sec                  5
#define idle_sec_before_disconnect 30

//#define DEBUG_eprintf(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#define DEBUG_eprintf(fmt, ...)
