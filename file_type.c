#include <string.h>

#include "file_type.h"
#include "config.h"

static char *ext(char *path)
{
	return strrchr(path, '.');
}

static int is_c(char *path)
{
	 char *e = ext(path);
	 if(!e)
		 return 0;
	if(!strcmp(e,".c")) {
	 DEBUG_eprintf("is_c:%s\n",e);
		return 1;
	}
	if(!strcmp(e,".h"))
		return 1;
	return 0;
}

static int is_cpp(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	char ex[][5] = { ".C", ".c++", ".cp", ".cxx",
		".CPP", ".cc", ".cpp", ".hh", ".hpp", "" };
	for(int i = 0; *ex[i]; ++i) {
		if(!strcmp(e, ex[i]))
			return 1;
	}
	return 0;
}

static int is_rust(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	if(!strcmp(e, ".rs"))
		return 1;
	return 0;
}

static int is_toml(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	if(!strcmp(e, ".toml"))
		return 1;
	return 0;
}

static int is_js(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	if(!strcmp(e, ".js"))
		return 1;
	return 0;
}

static int is_html(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	if(!strcmp(e, ".html"))
		return 1;
	return 0;
}

static int is_css(char *path)
{
	char *e = ext(path);
	if(!e)
		return 0;
	if(!strcmp(e, ".css"))
		return 1;
	return 0;
}

static int is_commit(char *path)
{
	char *fname = strrchr(path,'%');
	if(!fname++)
		return 0;
	if(!fname)
		return 0;
	char git[] = "COMMIT_EDITMSG";
	if(!strncmp(git, fname, strlen(git)))
		return 1;
	return 0;
}

static int is_none(char *path)
{
	return 1;
}

static int (*is_type[])(char *) = {

	[file_type_kind_c]       = is_c,
	[file_type_kind_cpp]     = is_cpp,
	[file_type_kind_rust]    = is_rust,
	[file_type_kind_toml]    = is_toml,
	[file_type_kind_html]    = is_html,
	[file_type_kind_js]      = is_js,
	[file_type_kind_css]     = is_css,
	[file_type_kind_commit]  = is_commit,
	[file_type_kind_none]    = is_none,
	[file_type_kind_max]     = 0,
};

enum file_type_kind file_type(char *path)
{
	enum file_type_kind t = 0;
	for(; t < file_type_kind_max; ++t) {
		if(is_type[t](path))
			return t;
	}
	/* unreachable */
	return file_type_kind_max;
}
