Neovim/Vim Discord Rich Presence for Linux

## Building

Obtain the header and library files from
https://github.com/discordapp/discord-rpc
drop only those files into `discord_lib`

```
$ make
$ ./rip-self
```

## Notes to users

Currently, this makefile only supports`libdiscord-rpc.a`

I might break the builds on the master branch at any time.
I do my best to tag any commit that I think is particularly stable.

## TODO
 - [x] disconnect from discord if no files are open within a time
 - [ ] daemonize
 - [ ] write a systemd wrapper
 - [x] if no project open and no commit line, display specal commit line
 - [x] give more intresting info on stdout
 - [x] use stdin for interaction
 - [x] current info goes into a log file
 - [ ] seperate the repl and data gathering portions into seperate
       applications

