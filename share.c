#include <string.h>
#include <stdio.h>
#include <sys/mman.h>
#include "config.h"
#include "share.h"

struct sha *init_share(void)
{
	struct sha *r;
	//TODO: use munmap before instructing any subprocess to exit
	//
	//NOTE: accessing r after one of the subprocess exits is
	//      undefined behavior
	r = mmap(
		&share_buf,
		sizeof(share_buf),
		PROT_READ | PROT_WRITE,
		MAP_SHARED | MAP_ANON,
		-1,
		0
	);
	//TODO: check mmap error
	r->run = 1;
	memset(r->file_list, 0, sizeof(r->file_list));
	return r;
}

