#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

FILE *fifo_out;
/*
 * Make and open a named fifo for writing.
 * Don't wait for the read end of the fifo.
 */
FILE *open_fifo(char *pathname, mode_t mode)
{
	if(mkfifo(pathname, mode)) {
		if(errno != EEXIST)
			return NULL;
	}
	return fopen(pathname, "w+");
}
/*
 * Remove the last output from the term and
 * print new input.
 */
void clr_print(FILE *fp, const char *s)
{
	/*
	 * \033[1G - Move to first col
	 * \033[2K - Del on line
	 * \033[A  - Move up one
	 */
	static int newlines = 0;
	fprintf(fp, "\033[1G\033[2K");
	while(newlines) {
		fprintf(fp, "\033[A\033[2K");
		--newlines;
	}
	const char *n = s;
	while((n = strchr(n, '\n'))) {
		++n;
		++newlines;
	}
	fprintf(fp, "%s", s);
	fflush(fp);
}
/*
int main(void)
{
	if(!(fifo_out = open_fifo("floop", 0777))) {
		perror("open_fifo()");
		return 1;
	}
	while(1) {
		clr_print(fifo_out,"oops");
		usleep(1000000);
		clr_print(fifo_out,"tag you're it!\ngre\ngrop\nplot");
		usleep(1000000);
	}
	return 0;
}
*/
