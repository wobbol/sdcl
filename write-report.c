#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <inttypes.h>

#define POLL_SEC         3 //XXX: this needs to come from the file being parsed
#define MAX_FILES       32
#define PROJECT_BUF_SZ  32
#define MAX_PROJECTS    16
#define MAX_INTERVAL    64 //TODO: this likely needs to be larger
//#define DEBUG_printf(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define DEBUG_printf(fmt, ...)

struct interval {
	time_t start;
	time_t end;
};

struct file {
	char *path;
	time_t total;
	struct interval i[MAX_INTERVAL];
};

struct project {
	char name[PROJECT_BUF_SZ];
	time_t total;
	struct file f[MAX_FILES];
	struct interval i[MAX_INTERVAL];
} project_list[MAX_PROJECTS] = {0};

struct ref {
	char *s;
	size_t len;
};

char *time_to_ascii(time_t in)
{
	static char buf[64];
	//strftime(buf, 64, "%F %T", gmtime(&in));
	strftime(buf, 64, "%F %T", localtime(&in));
	return buf;
}

char *human_time(time_t sec);
struct interval *get_next_interval(struct interval *listp);

int in_interval(struct file *f, struct interval inter)
{
	struct interval *j = f->i;
	for (int i = 0; i < MAX_INTERVAL; ++i, ++j) {
		if (
			j->start == inter.start &&
			j->end   == inter.end) {
			return 1;
		}
	}
	return 0;
}
void long_print_project(FILE *fp, struct project *p, struct interval inter)
{
	char *time = time_to_ascii(inter.start);

	fprintf(
		fp,
		"%s for: %s name: %s\n",
		time,
		human_time(inter.end - inter.start),
		p->name
	);
	struct file *f = p->f;
	for (int i = 0; i < MAX_FILES; ++i, ++f) {
		if (!f->path)
			break;
		if (!in_interval(f, inter))
			continue;
		fprintf(fp, "\t%s\n", f->path);
	}
	fprintf(fp,"\n");
}
struct interval *get_next_interval_r( struct interval *listp, struct interval **inter, int *index);
struct interval *get_next_interval(struct interval *listp) {
	static struct interval *j;
	static int i;
	if(listp) {
		j = listp;
		i = 0;
	}
	return get_next_interval_r(listp, &j, &i);
}
struct interval *get_next_interval_r(
	struct interval *listp,
	struct interval **inter,
	int *index)
{
	struct interval *j = *inter;
	int i = *index;
	if(listp) {
		j = listp;
		i = 0;
	}

	static struct interval ret;
	ret.start = j->start;
	ret.end = j->end;
	if (!ret.start)
		goto err;
	for(; i < MAX_INTERVAL; ++i, ++j) {
		if(!j->start)
			goto ok;
		if(ret.end + POLL_SEC > j->start) {
			ret.end = j->end;
			continue;
		}
		goto ok;
	}
err:
	return NULL;
ok:
	*inter = j;
	*index = i;
	return &ret;
}
void long_report(FILE *fp)
{
	//for each project
	// 	for each interval
	// 		print files matching interval
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (!*p->name)
			continue;
		struct interval *j = p->i;
		for (int k = 0; k < MAX_INTERVAL; ++k, ++j) {
			if (!j->start)
				break;
			long_print_project(fp, p, *j);
		}

	}
}
void per_file_report(FILE *fp)
{
	//for each project
	// 	for each file
	// 		print contiguous
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (!*p->name)
			continue;
		struct file *f = p->f;
		for (int k = 0; k < MAX_FILES; ++k, ++f) {
			if (!f->path)
				break;
			fprintf(fp, "%s\n", f->path);
			struct interval *j = get_next_interval(f->i);
			while (j) {
				fprintf(
					fp,
					"\t%s for: %s\n",
					time_to_ascii(j->start),
					human_time(j->end - j->start)
				);
				j = get_next_interval(NULL);
			}
			fprintf(fp, "\n");
		}

	}
}


void short_report(FILE *fp)
{
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (!*p->name)
			continue;
		fprintf(
			fp,
			"t: %s n: %s\n",
			human_time(p->total),
			p->name
		);
		/* Make the output easier to read. */
		int time_width = 0;
		struct file *f = p->f;
		for (int i = 0; i < MAX_FILES; ++i, ++f) {
			int width = strlen(human_time(f->total));
			if(width > time_width)
				time_width = width;
		}
		f = p->f;
		for (int i = 0; i < MAX_FILES; ++i, ++f) {
			if(!f->path)
				break;
			fprintf(
				fp,
				"\tt: %*s p: %s\n",
				time_width,
				human_time(f->total),
				f->path
			);
		}
	}
}

char *human_time(time_t time)
{
	const int sec  = 1;
	const int min  = sec  * 60;
	const int hour = min  * 60;
	const int day  = hour * 24;
	const int week = day  * 7;

	static char buf[64] = {0};
	static char p_buf[64] = {0};
	char *b = buf;

	if (time / week)
		goto w;
	if (time / day)
		goto d;
	if (time / hour)
		goto h;
	if (time / min)
		goto m;
	goto s;

w:
	sprintf(p_buf, "%1.d w ", time / week);
	b = stpcpy(b, p_buf);
	time = time % week;
d:
	sprintf(p_buf, "%2.2d d ", time / day);
	b = stpcpy(b, p_buf);
	time = time % day;
h:
	sprintf(p_buf, "%2.2d h ", time / hour);
	b = stpcpy(b, p_buf);
	time = time % hour;
m:
	sprintf(p_buf, "%2.2d m ", time / min);
	b = stpcpy(b, p_buf);
	time = time % min;
s:
	sprintf(p_buf, "%2.2d s", time);
	b = stpcpy(b, p_buf);

	return buf;
}

char *get_proj(char *path)
{
	char prefix[] = "/home/nicole/code/";
	static char buf[PROJECT_BUF_SZ];
	strncpy(buf, path + strlen(prefix), PROJECT_BUF_SZ);
	char *file_sep = strchr(buf, '/');
	*file_sep = '\0';
	return buf;
}
void add_interval_to_project(struct project *p, time_t start, time_t end)
{
	struct interval *j = p->i;
	for (int i = 0; i < MAX_INTERVAL; ++i, ++j)
	{
		if (j->start)
			continue;
		j->start = start;
		j->end   = end;
		break;
	}
}
void add_time_to_project(char *proj, time_t start, time_t end)
{
	time_t tot_time = end - start;
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (*p->name) {
			if (strcmp(p->name, proj))
				continue;
		}
		p->total += tot_time;
		add_interval_to_project(p, start, end);
		break;
	}
}
void add_interval_to_file(struct file *f, time_t start, time_t end)
{
	struct interval *j = f->i;
	for (int i = 0; i < MAX_INTERVAL; ++i, ++j)
	{
		if (j->start)
			continue;
		j->start = start;
		j->end   = end;
		break;
	}
}
void add_time_to_file(char *path, time_t start, time_t end)
{
	char *proj = get_proj(path);
	time_t tot_time = end - start;
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (*p->name)
			if (strcmp(p->name, proj))
				continue;
		struct file *f = p->f;
		for (int j = 0; j < MAX_FILES; ++j, ++f) {
			if (strcmp(path, f->path))
				continue;
			DEBUG_printf("srt: %llu end: %llu f->tot: %s tot: %llu\n", start, end, human_time(f->total), tot_time);
			f->total += tot_time;
			add_interval_to_file(f, start, end);
			break;
		}
		break;
	}
}
void add_path_to_project(char *path)
{
	char *proj = get_proj(path);
	struct project *p = project_list;
	for (int i = 0; i < MAX_PROJECTS; ++i, ++p) {
		if (!*p->name)
			strncpy(p->name, proj, sizeof(p->name));
		if (!strcmp(p->name, proj)) {
			struct file *f = p->f;
			for (int j = 0; j < MAX_FILES; ++j, ++f) {
				if (!f->path) {
					f->path = path;
					break;
				}
				if (!strcmp(path, f->path))
					break;
			}
			break;
		}
	}
}

enum parse_state {
	STATE_UNINIT,
	STATE_NAME,
	STATE_TIME,
	STATE_END,
	STATE_ERROR,
} parse_data_state;

int parse_data(struct ref *r)
{
	if (r->len < 1)
		goto end;

	size_t len = strlen(r->s);
	if (r->len - len < 1) {
		goto end;
	}

	r->s   += len;
	r->len -= len;
	if (!memcmp(r->s, "\0\0\0", 3) && r->len >= 3) {
		parse_data_state = STATE_NAME;
		r->s += 3;
		r->len -= 3;
		return 0;
	}
	if (!memcmp(r->s, "\0\0", 2) && r->len >= 2) {
		parse_data_state = STATE_TIME;
		r->s += 2;
		r->len -= 2;
		if (!r->len)
			goto end;
		return 0;
	}

	if (!memcmp(r->s, "\0", 1) && r->len >= 1) {
		++r->s;
		--r->len;
		return 0;
	}
	parse_data_state = STATE_ERROR;
	return -1;
end:
	parse_data_state = STATE_END;
	return -1;
}
void fill_project_list(struct ref r)
{
	parse_data_state = STATE_UNINIT;
	parse_data(&r);
	while (
		parse_data_state != STATE_END &&
		parse_data_state != STATE_ERROR
	) {
		char *name[MAX_FILES] = {0};
		char **n = name;
		int name_num = 0;
		while (parse_data_state == STATE_NAME) {
			add_path_to_project(r.s);
			*n = r.s;
			++n;
			++name_num;
			parse_data(&r);
		}
		/* assert(parse_data_state == STATE_TIME) */
		time_t start[MAX_FILES];
		int i = 0;
		while (parse_data_state == STATE_TIME && i < name_num) {
			start[i] = strtoull(r.s, NULL, 10);
			++i;
			parse_data(&r);
		}
		time_t end[MAX_FILES];
		i = 0;
		int ending = 0;
		while (parse_data_state == STATE_TIME) {
			if (i >= name_num)
				i = 0;
			end[i] = strtoull(r.s, NULL, 10);
			++i;
			parse_data(&r);
		}
		i = 0;
		n = name;
		while (i < name_num) {
			add_time_to_file(*n, start[i], end[i]);
			++i;
			++n;
		}
		/* add time to projects */
		i = 0;
		n = name;
		char proj[MAX_PROJECTS][PROJECT_BUF_SZ] = {0};
		int proj_num = 0;
		while (i < name_num) {
			char *p = get_proj(*n);
			for(int j = 0; j < name_num; ++j) {
				if (!strcmp(p, proj[j]))
					goto cont;
			}
			strncpy(proj[proj_num], get_proj(*n), PROJECT_BUF_SZ);
				++proj_num;
cont:
			++i;
			++n;
		}

		i = 0;
		while (i < proj_num) {
			add_time_to_project(proj[i], start[0], end[0]);
			++i;
		}

		/* assert(parse_data_state == STATE_NAME ||
		 * parse_data_state == STATE_END) */
		/* assert(i == name_num) */
	}
}

int main(void)
{
	FILE *fp = fopen("fugg.sdcl", "r");
	fseeko(fp, 0, SEEK_END);
	off_t flen = ftello(fp);
	char *file = mmap(0, flen, PROT_READ, MAP_SHARED, fileno(fp), 0);
	fclose(fp);
	struct ref s = {
		.s = file,
		.len = flen,
	};
	fill_project_list(s);
	short_report(stdout);
	long_report(stdout);
	per_file_report(stdout);
}
void debug_human_time(void)
{
	puts(human_time(0));
	puts(human_time(60-1));
	puts(human_time(60));
	puts(human_time(60*60-1));
	puts(human_time(60*60));
	puts(human_time(60*60*24-1));
	puts(human_time(60*60*24));
	puts(human_time(60*60*24*7-1));
	puts(human_time(60*60*24*7));
}

void wasteful_write_filter(char *s, off_t len, char from, char to)
{
	while (len--) {
		if (*s == from)
			fwrite(&to,1,1,stdout);
		else
			fwrite(s,1,1,stdout);
		++s;
	}
}

static int32_t lg2(float in)
{
	uint32_t ix;
	memcpy(&ix, &in, sizeof(in));
	uint32_t exp = (ix >> 23) & 0xff;
	int32_t log = (int32_t)(exp) - 127;
	return log;
}

static int print_len(long in)
{
	return (lg2(in)/lg2(10)) + 1;
}

void debug_data_parse(struct ref s)
{
	parse_data_state = STATE_UNINIT;
	char *state[] = {
		[STATE_UNINIT] = "uni",
		[STATE_NAME]   = "nam",
		[STATE_TIME]   = "tim",
		[STATE_END]    = "end",
		[STATE_ERROR]  = "err",
	};
	int ll = print_len(s.len);
	while (
		parse_data_state != STATE_END &&
		parse_data_state != STATE_ERROR
	) {
		parse_data(&s);
		printf("s: %s l:%*.1llu str: %s\n", state[parse_data_state], ll, s.len, s.s);
		fflush(stdout);
	}
	if (parse_data_state == STATE_END)
		ll = 0;
}
