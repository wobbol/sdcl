enum file_type_kind {
	file_type_kind_c,
	file_type_kind_cpp,
	file_type_kind_rust,
	file_type_kind_toml,
	file_type_kind_html,
	file_type_kind_js,
	file_type_kind_css,
	file_type_kind_commit,
	file_type_kind_none,
	file_type_kind_max
};
enum file_type_kind file_type(char *path);
