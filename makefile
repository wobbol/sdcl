all: rip-self report

CFLAGS = -g

SRC = main.c repl.c linenoise/linenoise.c share.c discord.c proj_parse.c file_type.c file-out.c util.c fifo-out.c
OBJ = ${SRC:.c=.o}

${OBJ}: ${SRC} config.h
	${CC} -o $@ ${CFLAGS} $*.c -c

report: write-report.c
	${CC} -o $@ ${CFLAGS} write-report.c

rip-self: ${OBJ} discord_lib/libdiscord-rpc.a
	g++ ${OBJ} discord_lib/libdiscord-rpc.a ${CFLAGS} -lpthread -o $@

config.h:
	cp config.def.h config.h

clean:
	rm ${OBJ}
	rm rip-self
