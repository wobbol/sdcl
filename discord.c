#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "discord_lib/discord_rpc.h"
#include "file_type.h"
#include "config.h"
#include "share.h"
#include "util.h"

FILE *fp_log;
static const char* APPLICATION_ID = "464627028751286273";

static void handleDiscordReady(const DiscordUser* connectedUser)
{
	fprintf(fp_log, "init: Discord: connected to user %s#%s - %s\n",
		connectedUser->username,
		connectedUser->discriminator,
		connectedUser->userId);
	fflush(fp_log);
}

static void handleDiscordDisconnected(int errcode, const char* message)
{
	fprintf(fp_log, "dnit: Discord: disconnected (%d: %s)\n", errcode, message);
	fflush(fp_log);
}

static void handleDiscordError(int errcode, const char* message)
{
	fprintf(fp_log, "erro: Discord: error (%d: %s)\n", errcode, message);
	fflush(fp_log);
}

static void init_discord()
{
    DiscordEventHandlers handlers = {0};
    handlers.ready = handleDiscordReady;
    handlers.disconnected = handleDiscordDisconnected;
    handlers.errored = handleDiscordError;
    Discord_Initialize(APPLICATION_ID, &handlers, 1, NULL);
}

//TODO: better name for this
static time_t set_time_on_idle_transition(void)
{
	static time_t last_edit_time = 0;
	static int is_idle = 0;
	if(!last_edit_time)
		last_edit_time = time(NULL);
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->is_open)
			continue;
		last_edit_time = time(NULL);
		is_idle = 0;
		break;
	}
	if(!f->is_open && !is_idle) {
		StartTime = time(NULL);
		is_idle = 1;
	}
	return last_edit_time;
}

//TODO: better name for this
static int disconnect_and_idle(void)
{
	time_t last_edit = set_time_on_idle_transition();
	int timeout = (last_edit + idle_sec_before_disconnect) < time(NULL);
	static int presence_active = 0;
	if(timeout && presence_active) {
		puts("Disconnecting");
		Discord_Shutdown();
		presence_active = 0;
	}
	if(!timeout && !presence_active) {
		init_discord();
		presence_active = 1;
		StartTime = time(NULL);
	}
	DEBUG_eprintf("Idle sec: %llu\n", time(NULL) - last_edit);
	return timeout;
}

static char *state(void)
{
	static char buf[PRJ_BUF_SZ];
	int left = PRJ_BUF_SZ;
	int editing = 0;
	char *ptr;
	struct file *f = share->file_list;

	ptr = stpncpy(buf, state_editing, left);
	left -= strlen(state_editing);
	for(int i = 0; i < MAX_OPEN_FILES && left > 0; ++i, ++f) {
		if(!f->is_open)
			continue;
		editing = 1;
		ptr += snprintf(ptr, left, "%s ", f->proj);
		left -= strlen(f->proj) + strlen(" ");
	}
	if(!editing)
		strncpy(buf, state_not_editing, PRJ_BUF_SZ);
	return buf;
}

static char *details(void)
{
	static char buf[PRJ_BUF_SZ];
	int left = PRJ_BUF_SZ;
	int committing = 0;
	char *ptr;
	struct file *f = share->file_list;

	ptr = stpncpy(buf, committing_header, left);
	left -= strlen(state_editing);
	for(int i = 0; i < MAX_OPEN_FILES && left > 0; ++i, ++f) {
		if(!f->is_open)
			continue;
		if(strcmp(f->name, "COMMIT_EDITMSG"))
			continue;
		committing = 1;
		ptr += snprintf(ptr, left, "%s ", f->proj);
		left -= strlen(f->proj) + strlen(" ");
	}
	if(!committing)
		*buf = '\0';
	return buf;
}

static char *large_image_name(void)
{
	char *dict_type_img[] = {
		[file_type_kind_c]       = "c-img",
		[file_type_kind_cpp]     = "cpp-img",
		[file_type_kind_rust]    = "rust-img",
		[file_type_kind_toml]    = "haggis",
		[file_type_kind_html]    = "haggis",
		[file_type_kind_js]      = "haggis",
		[file_type_kind_css]     = "haggis",
		[file_type_kind_commit]  = "haggis",
		[file_type_kind_none]    = "haggis",
		[file_type_kind_max]     = 0,
	};
	int type_count[file_type_kind_max] = {0};
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->is_open)
			continue;
		type_count[file_type(f->name)] += 1;
	}

	/* get the image for the file type with the most files open */
	int *max = type_count;
	int *b = type_count + 1;
	for(; b < &type_count[file_type_kind_max]; ++b) {
		if(*max < *b) {
			max = b;
		}
	}
	return dict_type_img[max - type_count];
}

static char *small_image_name(void)
{
	return "baboon";
}

static void updateDiscordPresence()
{
	DiscordRichPresence discordPresence;
	memset(&discordPresence, 0, sizeof(discordPresence));

	int timed_out = disconnect_and_idle();

	if(timed_out) {
		//printf(".");
		//fflush(stdout);
	} else {
		discordPresence.state          = state();
		discordPresence.details        = details();
		discordPresence.largeImageKey  = large_image_name();
		discordPresence.smallImageKey  = small_image_name();
		discordPresence.startTimestamp = StartTime;
		Discord_UpdatePresence(&discordPresence);
		//info(stdout, p);
	}
}

static void discord_handle_sig(int sig)
{
	Discord_Shutdown();
	//sleep(1); //XXX: Fix race condition and remove this.
	exit(0);
}

void discord_main(FILE *fp)
{
	fp_log = fp;
	sigint_handle_reg(discord_handle_sig);
	StartTime = time(NULL);
	while (share->run) {
		fprintf(fp_log, "poll: discord\n");
		updateDiscordPresence();
		fflush(NULL);
		sleep(sleep_sec);

#ifdef DISCORD_DISABLE_IO_THREAD
		Discord_UpdateConnection();
#endif
		Discord_RunCallbacks();
	}

	Discord_Shutdown();
}

//TODO: add FILE argument
void print_presence(void)
{
	printf("state:     %s\n", state());
	printf("details:   %s\n", details());
	printf("large-img: %s\n", large_image_name());
	printf("small-img: %s\n", small_image_name());
	printf("startTime: %s\n", StartTime);
}
