#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "util.h"
#include "config.h"
#include "share.h"

FILE *fp;

/* return 0 on success */
static int fseek_back(off_t offset, int whence)
{
	fseeko(fp, 0, whence);
	off_t len = ftello(fp);
	return fseeko(fp, len - offset, SEEK_SET);

}

static void print_file(void)
{
	char time_s[16] = {0};
	struct file *f = share->file_list;
	int is_open = 0;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(f->is_open) {
			is_open = 1;
			break;
		}
	}
	if(!is_open)
		return;
	static uintmax_t open_list[MAX_OPEN_FILES];
	uintmax_t *c = open_list;
	int open_files_changed = 0;
	f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f, ++c) {
		if(*c == f->uniq)
			continue;
		*c = f->uniq;
		open_files_changed = 1;
	}
	if(open_files_changed) {
		fwrite("\0", 1, 1, fp);
		/* assert(file_end_present()) */
		f = share->file_list;
		for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
			if(!f->is_open)
				continue;
			fwrite(f->path, 1, strlen(f->path) + 1, fp);
		}
		fwrite("\0", 1, 1, fp);
	}

	f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->is_open)
			continue;
		snprintf(time_s, 16, "%llu", time(NULL));
		fwrite(time_s, 1, strlen(time_s) + 1, fp);
		fflush(fp);
	}
		fwrite("\0", 1, 1, fp);
		fflush(fp);
}

static FILE *create_rw_file(char *path)
{
	int fd = open(path, O_RDWR | O_CREAT, 0644);
	if(fd < 0)
		return NULL;
	return fdopen(fd, "r+");
}

static int file_end_present(void)
{
	fseeko(fp, 0, SEEK_END);
	off_t len = ftell(fp);
	if(!len) {
		fwrite("\0\0", 1, 2, fp);
		fseeko(fp, 0, SEEK_END);
		len = ftell(fp);
		return 1;
	}
	fseeko(fp, len - 1, SEEK_SET);
	char f_end[4];
	fgets(f_end,4,fp);
	if(memcmp(f_end, "\0\0", 2))
		return 0;
	return 1;
}

static void sighand()
{
	if(file_end_present()) {
		exit(0);
	} else {
		fseek_back(2, SEEK_END);
		char f_end[4];
		fgets(f_end,4,fp);
		if(memcmp(f_end, "\0\0", 2)) {
			fprintf(stderr, "unrecoverable out file state\n");
			exit(0);
		}
		fseeko(fp, 0, SEEK_END);
		fprintf(fp,"\0");
		exit(0);
	}
}

void file_out_main(FILE *log_fp)
{
	sigint_handle_reg(sighand);
	fp = create_rw_file("fugg.sdcl");
	if(!fp) {
		perror("create_rw_file()");
		return;
	}
	if(!file_end_present()) {
		fprintf(stderr, "unterminated file\n");
		return;
	}
	fseeko(fp, 0, SEEK_END);

	while(share->run) {
		fprintf(log_fp, "poll: file out\n");
		print_file();
		fflush(NULL);
		sleep(2);
	}
	return;
}
