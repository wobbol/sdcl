#define CF_MASK 0x100
int lock(int *loc)
{
	int bit_pos = 0;
	short f = CF_MASK;
	__asm__ __volatile__(
		"lock bts %[bit_pos], %[lock]\n\t"
		"lahf\n\t"
		"mov %%ax, %[fg]\n"
		:[lock] "+m" (*loc), [bit_pos] "+r" (bit_pos),  [fg] "+r" (f)
	);
	return f & CF_MASK;
}
int main(void)
{
	int p = 0;
	printf("%d\n", p);
	while(lock(&p))
		;
	printf("%d\n", p);
	/* spin_lock(&p); this will hang the process */
	while(lock(&p))
	printf("%d\n", p);
	return 0;
}
