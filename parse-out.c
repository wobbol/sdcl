#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>

#include "config.h"
#include "share.h"
#include "util.h"

#define BUF_REASON 128

int seek_line(FILE *f, off_t num, int whence)
{
	if(!num)
		return 1;
	fseeko(f, 0, whence);
	off_t t = ftello(f);
	int misalign = t % BUF_REASON;
	if(misalign) {
		fseeko(f, t - misalign, SEEK_SET);
	}
	if(fseeko(f, BUF_REASON * num, SEEK_CUR))
		perror("fseeko()");
	return 1;
}
/* use strfile(8) */
int seek_project(FILE *f, char *proj_name)
{
	char buf[FILENAME_MAX];
	fseeko(f, 0, SEEK_SET);
	*buf = '\0';
	while(!strncmp(proj_name, buf, FILENAME_MAX)) {
		if(!fgets(buf, FILENAME_MAX, f))
			return 0;
	}
	return 1;
}
void reason_fprintf(FILE *fp, char *fmt, ...)
{
	char out[BUF_REASON];
	char in[BUF_REASON];
	memset(out, ' ', BUF_REASON);
	out[BUF_REASON - 2] = '\n';
	out[BUF_REASON - 1] = '\0';

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(in, BUF_REASON, fmt, ap);
	va_end(ap);
	memcpy(out, in, strlen(in));
	fprintf(fp, "%s", out);
}

void print(FILE *fp)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->is_open)
			continue;
		seek_line(fp, f->line, SEEK_SET);
		reason_fprintf(fp, "%s %llu %llu", f->path, f->start, f->len);
	}
	fflush(fp);
}
static void assign_line_numbers(void)
{
	static off_t unassigned = 1;
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->is_open || f->line)
			continue;
		f->line = unassigned++;
	}
}
static void signal_handler(int sig)
{
	exit(0);
}
void file_out_main(void)
{
	sigint_handle_reg(signal_handler);
	int fd = open(work_log_path, O_RDWR | O_CREAT, 0644);
	if(fd < 0) {
		fprintf(stderr, "open() %s: %s\n", work_log_path, strerror(errno));
		exit(1);
	}

	FILE *fp = fdopen(fd, "r+");
	if(!fp) {
		fprintf(stderr, "fopen() %s: %s\n", work_log_path, strerror(errno));
		exit(1);
	}
	fseeko(fp, 0, SEEK_END);
	if(!ftello(fp)) {
		reason_fprintf(fp,"path\tstart_time\telapsed_time");
	}
	while(share->run) {
		printf("fil\r\n");
		assign_line_numbers();
		print(fp);
		sleep(2);
	}
}
