#include <stdio.h>
#include <signal.h>

void sigint_handle_reg(void (*h)(int sig))
{
	struct sigaction sa;
	sa.sa_handler = h;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if(sigaction(SIGINT, &sa, NULL) == -1)
		perror("could not register SIGINT handler");
}
