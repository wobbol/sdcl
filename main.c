#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "repl.h"
#include "discord.h"
#include "proj_parse.h"
#include "file-out.h"
#include "fifo-out.h"
#include "share.h"

pid_t pid_disc;
pid_t pid_file_out;
pid_t pid_proj_parse;

static void _Noreturn handle_sig(int sig)
{
	puts("\nExiting.\n");
	kill(pid_disc, sig);
	kill(pid_file_out, sig);
	kill(pid_proj_parse, sig);
	exit(0);
}

static void _Noreturn die(char *s)
{
	perror(s);
	handle_sig(SIGINT);
}

static pid_t fork_exec_or_die(void (*m)(FILE *fp), FILE *fifo)
{
	pid_t pid = fork();
	switch(pid) {
		case -1: die("fork()"); break;
		case  0: m(fifo);       break;
	}
	return pid;
}

int main(void)
{
	FILE *log = open_fifo("smp_debug", 0644);
	fprintf(log, "init: shared memory and log ...");
	fflush(NULL);
	share = init_share();
	fprintf(log, " done!\n");
	fflush(NULL);
	pid_disc = fork_exec_or_die(discord_main, log);
	pid_proj_parse = fork_exec_or_die(proj_parse_main, log);
	pid_file_out = fork_exec_or_die(file_out_main, log);
	//pid_t pid_fifo = lanuch(fifo_main);
	//pid_t pid_file = lanuch(file_main);
	return repl_main(handle_sig);
}
