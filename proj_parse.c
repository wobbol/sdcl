#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#include "config.h"
#include "proj_parse.h"
#include "share.h"
#include "util.h"

static void strchr_replace(char *s, char searchfor, char replacewith)
{
	while((s = strchr(s, searchfor))) {
		*s = replacewith;
		++s;
	}
}

static int lsof_parse(FILE *stream, char **path, char **proj_name, char **file_name)
{
	if(!stream)
		return -2;
	static char buf[FILENAME_MAX];
	if(!fgets(buf, sizeof buf, stream))
		return -1;
	if(buf[0] != 'n')
		return 1;
	if(!(*path = strchr(buf, '%'))) /* remove swap directory */
		return 1;
	*strrchr(*path, '.') = '\0'; /* remove .swp */
	*file_name = strrchr(buf, '%') + 1;
	static char p[FILENAME_MAX];
	strncpy(p, *path, FILENAME_MAX - 1);
	p[FILENAME_MAX - 1] = '\0';
	*proj_name = strtok(p + strlen(code_path), "%");
	return 0;
}

/* fill share->file_list */
static int currently_open(char *path)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!f->was_open)
			continue;
		if(strncmp(f->path, path, FILENAME_MAX))
			continue;
		f->len = time(NULL) - f->start;
		f->is_open = 1;
		return 1;
	}
	return 0;
}
static int file_add(char *path, char *proj, char *file)
{
	static uintmax_t uniq = 0;
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(f->is_open)
			continue;
		strncpy(f->path, path, FILENAME_MAX);
		strncpy(f->proj, proj, FILENAME_MAX);
		strncpy(f->name, file, FILENAME_MAX);
		f->start = time(NULL);
		f->is_open = 1;
		f->uniq = ++uniq;
		return 1;
	}
	return 0;
}

static void clear_closed_files(void)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!(f->was_open && !f->is_open))
			continue;
		/* f->is_open = 0; */
		f->len = 0;
		f->line = 0;
		/* memset(f->name, 0, FILENAME_MAX); */
		/* memset(f->path, 0, FILENAME_MAX); */
		/* memset(f->proj, 0, FILENAME_MAX); */
		f->start = 0;
		f->uniq = 0;
		/* f->was_open = 0; */
	}
}

static void push_back_open_status(void)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		f->was_open = f->is_open;
	}
}

static void set_all_not_open(void)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		f->is_open = 0;
	}
}

static void update_file_list(void)
{
	FILE *lsof = popen(popen_command, "r");
	set_all_not_open();
	int n = 0;
	while(n < MAX_OPEN_FILES) {
		char *path, *file, *proj;
		switch(lsof_parse(lsof, &path, &proj, &file)) {
			case -1: goto file_ended;
			case 1:  continue;
		}
	strchr_replace(path, '%', '/');
		if(currently_open(path))
			continue;
		if(!file_add(path, proj, file))
			break;
	}
file_ended:
	pclose(lsof);
	clear_closed_files();
	push_back_open_status();
}

static void proj_parse_handle_sig(int sig)
{
	exit(0);
}

void proj_parse_main(FILE *fp)
{
	sigint_handle_reg(proj_parse_handle_sig);
	close(STDERR_FILENO); //XXX: ignore lsof error messages
	while(share->run) {
		fprintf(fp, "poll: proj parse\n");
		update_file_list();
		fflush(NULL);
		sleep(3);
	}
}
void print_file_list(void)
{
	struct file *f = share->file_list;
	for(int i = 0; i < MAX_OPEN_FILES; ++i, ++f) {
		if(!(f->is_open || f->was_open))
			continue;
		printf("file { path:%s name: %s proj: %s is: %d was: %d start: %llu len: %llu line: %llu }\n",
			f->path,
			f->name,
			f->proj,
			f->is_open,
			f->was_open,
			f->start,
			f->len,
			f->line
		);
	}
	puts("end");
	fflush(stdout);
}
